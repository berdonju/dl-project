# Projet

Train diverse models for the classification of cat ond dog breeds

## Dataset

Pets dataset provides the breeds of a large variety of pets https://www.robots.ox.ac.uk/~vgg/data/pets/

## Exctract data

In the project, images and annotations are not extracted from there respecting files images.tar.gz and annotations.tar.gz. To exctract them you must run the first code of the file ImgVisualization.ipynb that will create two folders : Extracted_images and Extracted_annotations

PS : For the moment, I haven't been able to upload the images.tar.gz file. It can be downloaded by using the link in the 'dataset' paragraph

## Saved models names

Models are namde this way : ModelName_TxCyOz
with : x the id of the transformation, y the id of the loss function and z the id of the optimizer. 
Models are stored in the folder res

PS : Problems to umpload models on GitLab

## Train model

Each model has its own training file. Before running the code, you can choose the parameters : transfomation, loss function and optimizer where we can have more details in the file settings.py, and write the good saved model name.

## Test model

Each model has its own testing file. A file is composed of many blocks, one for every saved model.
Results can be visualized in four ways : 
- Global accuracy
- Breeds accuracy
- Confusion matrix
- Error confusion matrix
